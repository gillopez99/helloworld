package com.Gilberto.HolaMundos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolaMundosApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolaMundosApplication.class, args);
	}

}
